import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    id("java")
    id(Dependencies.Plugins.SpringBoot) version Versions.Plugins.SpringBoot apply false
    id(Dependencies.Plugins.SpringDependencyManagement) version Versions.Plugins.SpringDependencyManagement
    kotlin(Dependencies.Plugins.Kotlin.JVM) version Versions.Plugins.Kotlin
    kotlin(Dependencies.Plugins.Kotlin.Spring) version Versions.Plugins.Kotlin
}

group = "com.langnerd"
version = "0.0.1-SNAPSHOT"

repositories {
    mavenCentral()
}

subprojects {
    apply(plugin = "java")
    java {
        sourceCompatibility = JavaVersion.VERSION_11
        targetCompatibility = JavaVersion.VERSION_11
    }

    if (this.name != "common") {
        apply(plugin = Dependencies.Plugins.SpringBoot)
        apply(plugin = Dependencies.Plugins.SpringDependencyManagement)
    }

    repositories {
        mavenCentral()
    }
    dependencies {
        // Spring Boot
        implementation(Dependencies.SpringBoot.Security)
        implementation(Dependencies.SpringBoot.Thymeleaf)
        implementation(Dependencies.SpringBoot.WebFlux)

        // Templating engine
        implementation(Dependencies.ThymeleafExtras)

        // Kotlin
        implementation(Dependencies.Kotlin.Jackson)
        implementation(Dependencies.Kotlin.JDK8)
        implementation(Dependencies.Kotlin.Reflect)

        // Kotlin Extensions
        implementation(Dependencies.KotlinX.Reactor)
        implementation(Dependencies.KotlinX.ReactorCoroutines)

        // Test
        testImplementation(Dependencies.Test.SpringBoot)
        testImplementation(Dependencies.Test.SpringSecurity)
        testImplementation(Dependencies.Test.Reactor)
    }
    tasks.withType<KotlinCompile> {
        kotlinOptions {
            freeCompilerArgs = listOf("-Xjsr305=strict")
            jvmTarget = "11"
        }
    }

    tasks.withType<Test> {
        useJUnitPlatform()
    }
}

