# Episode 1 - Form Based Authentication

Objective: Add form based authentication with minimum effort.

* [Watch on YouTube](https://youtu.be/hvAWwpow7_A)
* [Read a blog post](https://www.langnerd.com/form-authentication-with-spring-webflux/)

## How to Build and Run the Project

From the project root run the following command to build the project:
```
./gradlew :webflux-security-part-1:build
```

In order to run the application:
```
java -jar ./webflux-security-part-1/build/libs/webflux-security-part-1.jar
```

## Reference Documentation and Guides
For further reference, please consider the following articles and guides:

* [Spring Security](https://docs.spring.io/spring-security/reference/)
* [Spring Security with WebFlux](https://docs.spring.io/spring-security/reference/reactive/configuration/webflux.html)
* [Spring WebFlux](https://docs.spring.io/spring-framework/docs/current/reference/html/web-reactive.html)
* [Thymeleaf](https://docs.spring.io/spring-boot/docs/2.5.6/reference/htmlsingle/#boot-features-spring-mvc-template-engines)
* [Handling Form Submission](https://spring.io/guides/gs/handling-form-submission)
