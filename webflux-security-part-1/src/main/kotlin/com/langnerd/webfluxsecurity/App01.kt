package com.langnerd.webfluxsecurity

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class App01

fun main(args: Array<String>) {
	runApplication<App01>(*args)
}
