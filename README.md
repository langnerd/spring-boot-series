# Spring Boot: WebFlux and Security

Tutorial on Spring Security applied to a reactive web stack on WebFlux.

### [Watch on YouTube](https://www.youtube.com/watch?v=WGKsPJBrZBE&list=PLfTsqnmRhruE1p4ALzzU3md5_IXHZX8eL)

## Content
* [Introduction](#introduction)
* [Episode 1: Form Based Authentication](#episode-1-form-based-authentication)
* [Episode 2: Custom User Object and Loading Users from a Database](#episode-2-custom-user-object-and-loading-users-from-a-database)
* [Episode 3: Custom Login Page](#episode-3-custom-login-page)
* [Episode 4: Social Login via Google and Facebook](#episode-4-social-login-via-google-and-facebook)
* [Episode 5: Magic Link](#episode-5-magic-link)
* [Episode 6: Mitigation of Common Security Threats](#episode-6-mitigation-of-common-security-threats)
* [Episode 7: Two-Factor Authentication](#episode-7-two-factor-authentication)
* [How to Build and Run the Project](#how-to-build-and-run-the-project)

## Introduction
What will you learn and why.

* [Watch on YouTube](https://youtu.be/UTCp622JuLw)
* [Read a blog post](https://www.langnerd.com/new-series-on-spring-security-and-webflux/)
* [Slides](https://slides.com/tomaszezula/spring-security-webflux/)

## Episode 1: Form Based Authentication
Learn how to add form based authentication with minimum effort.

* [Watch on YouTube](https://youtu.be/hvAWwpow7_A)
* [Read a blog post](https://www.langnerd.com/form-authentication-with-spring-webflux/)
* [Source Code](webflux-security-part-1)

## Episode 2: Custom User Object and Loading Users from a Database
Customize the user object by adding properties such as first and last name, or an email address and load users from a database.

* [Watch on YouTube](https://youtu.be/WGKsPJBrZBE)
* [Read a blog post](https://www.langnerd.com/spring-security-webflux-load-users-from-a-database/)
* [Source Code](webflux-security-part-2)

## Episode 3: Custom Login Page
Add a custom login page and handle login / logout in your code.

* [Watch on YouTube](https://youtu.be/pyh4B_GfJFA)
* [Read a blog post](https://www.langnerd.com/spring-security-webflux-custom-login-form/)
* [Source Code](webflux-security-part-3)

## Episode 4: Social Login via Google and Facebook
Add a social login via Google and Facebook.

* Watch on YouTube
* Read a blog post
* [Source Code](webflux-security-part-4)

## Episode 5: Magic Link
Add a password-less login via email.

* Watch on YouTube
* Read a blog post
* [Source Code](webflux-security-part-5)

## Episode 6: Mitigation of Common Security Threats
Shield against some common security threats described by OWASP Top Ten.

* Watch on YouTube
* Read a blog post
* [Source Code](webflux-security-part-6)

## Episode 7: Two-Factor Authentication
Add an option to use two-factor authentication (2FA).

* Watch on YouTube
* Read a blog post
* [Source Code](webflux-security-part-7)

## How to Build and Run the Project

**Prerequisites**
* Java 11 or newer
* Git client
* Some sort of IDE (IntelliJ, Visual Studio Code etc.)

Download the source code or clone this repository. In the project directory on your local machine build the project:
```
./gradlew clean build
```

This will download all dependencies and compile the source code.

Each episode is a standalone web application. Check the respective README to see how to run the app:
* [Episode 1](webflux-security-part-1)
* [Episode 2](webflux-security-part-2)
* [Episode 3](webflux-security-part-3)
* [Episode 4](webflux-security-part-4)
* [Episode 5](webflux-security-part-5)
* [Episode 6](webflux-security-part-6)
* [Episode 7](webflux-security-part-7)


