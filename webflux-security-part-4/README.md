# Episode 4 - Social Login

Objective: Add a social login via Google and Facebook.

Coming soon!:
* YouTube
* Blog Post

## How to Build and Run the Project

From the project root run the following command to build the project:
```
./gradlew :webflux-security-part-4:build
```

In order to run the application:
```
java -jar ./webflux-security-part-4/build/libs/webflux-security-part-4.jar
```

### Reference Documentation and Guides
For further reference, please consider the following articles and guides:

* [Spring Security](https://docs.spring.io/spring-boot/docs/2.5.6/reference/htmlsingle/#boot-features-security)
* [Spring Security with WebFlux](https://docs.spring.io/spring-security/reference/reactive/configuration/webflux.html)
* OpenID Connect Providers:
  * [Google](https://developers.google.com/identity/protocols/oauth2/openid-connect)
  * Facebook
  * Twitter

