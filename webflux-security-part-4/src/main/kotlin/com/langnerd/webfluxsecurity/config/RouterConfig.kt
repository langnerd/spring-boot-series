package com.langnerd.webfluxsecurity.config

import com.langnerd.webfluxsecurity.dto.Response
import com.langnerd.webfluxsecurity.handler.Handler
import com.langnerd.webfluxsecurity.handler.IndexHandler
import com.langnerd.webfluxsecurity.handler.WebAdapter
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.web.reactive.function.server.router

@Configuration
class RouterConfig(private val webAdapter: WebAdapter) {

    @Bean
    fun routes(indexHandler: IndexHandler) = router {
        GET("/", indexHandler.adapt())
        GET("/login") { Response.LoginForm }
    }

    private fun <T : Handler> T.adapt() = webAdapter.adapt(this::handle)
}

    