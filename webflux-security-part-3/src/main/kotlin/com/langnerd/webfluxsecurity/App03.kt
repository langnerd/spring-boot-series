package com.langnerd.webfluxsecurity

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.context.properties.ConfigurationPropertiesScan
import org.springframework.boot.runApplication

@SpringBootApplication
@ConfigurationPropertiesScan
class App03

fun main(args: Array<String>) {
	runApplication<App03>(*args)
}
