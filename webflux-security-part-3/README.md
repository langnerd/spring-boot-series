# Episode 3 - Custom Login Page

Objective: Add a custom login page and handle login / logout in your code.

Coming soon!:
* YouTube
* Blog Post

## How to Build and Run the Project

From the project root run the following command to build the project:
```
./gradlew :webflux-security-part-3:build
```

In order to run the application:
```
java -jar ./webflux-security-part-3/build/libs/webflux-security-part-3.jar
```

### Reference Documentation and Guides
For further reference, please consider the following articles and guides:

* [Spring Security](https://docs.spring.io/spring-security/reference/)
* [Spring Security with WebFlux](https://docs.spring.io/spring-security/reference/reactive/configuration/webflux.html)
* [Spring WebFlux](https://docs.spring.io/spring-framework/docs/current/reference/html/web-reactive.html)
* [Thymeleaf](https://docs.spring.io/spring-boot/docs/2.5.6/reference/htmlsingle/#boot-features-spring-mvc-template-engines)
