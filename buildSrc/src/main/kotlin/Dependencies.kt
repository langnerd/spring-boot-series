object Dependencies {

    // Kotlin
    object Kotlin {
        const val Jackson = "com.fasterxml.jackson.module:jackson-module-kotlin:${Versions.Jackson}"
        const val Reflect = "org.jetbrains.kotlin:kotlin-reflect"
        const val JDK8 = "org.jetbrains.kotlin:kotlin-stdlib-jdk8"

    }

    // Kotlin extensions
    object KotlinX {
        const val Reactor = "io.projectreactor.kotlin:reactor-kotlin-extensions:${Versions.KotlinX.Reactor}"
        const val ReactorCoroutines = "org.jetbrains.kotlinx:kotlinx-coroutines-reactor:${Versions.KotlinX.Coroutines}"
    }

    object Persistence {
        const val H2 = "com.h2database:h2:${Versions.Persistence.H2}"
        const val R2DBC_H2 = "io.r2dbc:r2dbc-h2:${Versions.Persistence.R2DBC_H2}"
    }

    object Plugins {
        const val SpringBoot = "org.springframework.boot"
        const val SpringDependencyManagement = "io.spring.dependency-management"
        object Kotlin {
            const val JVM = "jvm"
            const val Spring = "plugin.spring"
        }
    }

    const val ServletApi = "javax.servlet:javax.servlet-api:${Versions.ServletApi}"

    // Spring Boot modules
    object SpringBoot {
        const val DevTools = "org.springframework.boot:spring-boot-devtools:${Versions.SpringBoot}"
        const val Security = "org.springframework.boot:spring-boot-starter-security:${Versions.SpringBoot}"
        const val Thymeleaf = "org.springframework.boot:spring-boot-starter-thymeleaf:${Versions.SpringBoot}"
        const val WebFlux = "org.springframework.boot:spring-boot-starter-webflux:${Versions.SpringBoot}"
        const val R2DBC = "org.springframework.boot:spring-boot-starter-data-r2dbc:${Versions.SpringBoot}"
        const val OAuth2Client = "org.springframework.boot:spring-boot-starter-oauth2-client:${Versions.SpringBoot}"
        const val Validation = "org.springframework.boot:spring-boot-starter-validation:${Versions.SpringBoot}"
        const val Mail = "org.springframework.boot:spring-boot-starter-mail:${Versions.SpringBoot}"
        const val Session = "org.springframework.session:spring-session-core:${Versions.SpringBootSession}"
    }

    object MultiFactorAuthentication {
        const val OTP = "com.github.bastiaanjansen:otp-java:${Versions.MultiFactorAuthentication.OTP}"
    }

    // Test dependencies
    object Test {
        const val SpringBoot = "org.springframework.boot:spring-boot-starter-test:${Versions.SpringBoot}"
        const val SpringSecurity = "org.springframework.security:spring-security-test:${Versions.SpringSecurity}"
        const val Reactor = "io.projectreactor:reactor-test:${Versions.ReactorTest}"
    }

    // Templating engine
    const val ThymeleafExtras = "org.thymeleaf.extras:thymeleaf-extras-springsecurity5:${Versions.Thymeleaf}"

}