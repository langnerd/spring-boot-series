object Versions {

    const val Jackson = "2.13.0"

    object KotlinX {
        const val Reactor = "1.1.5"
        const val Coroutines = "1.5.2-native-mt"
    }

    object MultiFactorAuthentication {
        const val OTP = "1.2.3"
    }

    object Persistence {
        const val H2 = "1.4.200"
        const val R2DBC_H2 = "0.8.4.RELEASE"
    }

    object Plugins {
        const val SpringBoot = "2.6.2"
        const val SpringDependencyManagement = "1.0.11.RELEASE"
        const val Kotlin = "1.6.10"
    }

    const val ReactorTest = "3.4.12"

    const val SpringBoot = "2.6.2"
    const val SpringBootSession = "2.6.0"

    const val SpringSecurity = "5.5.1"

    const val ServletApi = "3.1.0"

    const val Thymeleaf = "3.0.4.RELEASE"

}