rootProject.name = "webflux-security-tutorial"

include(
    "common",
    "webflux-security-part-1",
    "webflux-security-part-2",
    "webflux-security-part-3",
    "webflux-security-part-4",
    "webflux-security-part-5",
    "webflux-security-part-6",
    "webflux-security-part-7"
)
