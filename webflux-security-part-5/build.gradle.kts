plugins {
	kotlin(Dependencies.Plugins.Kotlin.JVM)
	kotlin(Dependencies.Plugins.Kotlin.Spring)
}

dependencies {
	// Spring Boot modules
	implementation(Dependencies.SpringBoot.OAuth2Client)
	implementation(Dependencies.SpringBoot.R2DBC)
	implementation(Dependencies.SpringBoot.Validation)
	implementation(Dependencies.SpringBoot.Mail)

	implementation(Dependencies.ServletApi)

	// Persistence
	runtimeOnly(Dependencies.Persistence.H2)
	runtimeOnly(Dependencies.Persistence.R2DBC_H2)

	implementation(project(":common"))
}
