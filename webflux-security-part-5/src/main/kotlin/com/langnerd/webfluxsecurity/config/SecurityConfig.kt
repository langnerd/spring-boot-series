package com.langnerd.webfluxsecurity.config

import com.langnerd.webfluxsecurity.filter.TokenAuthenticationFilter
import com.langnerd.webfluxsecurity.service.EmailTokenService
import org.springframework.context.annotation.Bean
import org.springframework.http.HttpMethod
import org.springframework.security.authentication.ReactiveAuthenticationManager
import org.springframework.security.authentication.UserDetailsRepositoryReactiveAuthenticationManager
import org.springframework.security.config.annotation.web.reactive.EnableWebFluxSecurity
import org.springframework.security.config.web.server.SecurityWebFiltersOrder
import org.springframework.security.config.web.server.ServerHttpSecurity
import org.springframework.security.core.userdetails.ReactiveUserDetailsService
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.security.web.server.SecurityWebFilterChain
import org.springframework.security.web.server.context.WebSessionServerSecurityContextRepository
import org.springframework.security.web.server.util.matcher.ServerWebExchangeMatchers


@EnableWebFluxSecurity
class SecurityConfig(
    private val userDetailsService: ReactiveUserDetailsService,
    private val emailTokenService: EmailTokenService
) {

    @Bean
    fun springSecurityFilterChain(http: ServerHttpSecurity): SecurityWebFilterChain =
        http.csrf().disable()
            .authorizeExchange()
            .pathMatchers("/login/**", "/*.css").permitAll()
            .anyExchange().authenticated()
            .and().authenticationManager(authenticationManager())
            .addFilterBefore(tokenAuthenticationFilter(), SecurityWebFiltersOrder.AUTHENTICATION)
            .formLogin().loginPage("/login")
            .and().oauth2Login()
            .and().logout()
            .requiresLogout(ServerWebExchangeMatchers.pathMatchers(HttpMethod.GET, "/logout"))
            .and().build()

    private fun tokenAuthenticationFilter(): TokenAuthenticationFilter =
        TokenAuthenticationFilter(emailTokenService,  WebSessionServerSecurityContextRepository())

    private fun authenticationManager(): ReactiveAuthenticationManager {
        val authenticationManager = UserDetailsRepositoryReactiveAuthenticationManager(userDetailsService)
        authenticationManager.setPasswordEncoder(passwordEncoder())
        return authenticationManager
    }

    private fun passwordEncoder(): PasswordEncoder =
        BCryptPasswordEncoder(10)
}