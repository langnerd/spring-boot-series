package com.langnerd.webfluxsecurity.config

import com.langnerd.webfluxsecurity.dto.Response.Companion.EmailLoginForm
import com.langnerd.webfluxsecurity.dto.Response.Companion.IndexRedirect
import com.langnerd.webfluxsecurity.dto.Response.Companion.LoginForm
import com.langnerd.webfluxsecurity.handler.email.EmailLoginHandler
import com.langnerd.webfluxsecurity.handler.Handler
import com.langnerd.webfluxsecurity.handler.IndexHandler
import com.langnerd.webfluxsecurity.handler.WebAdapter
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.web.reactive.function.server.router

@Configuration
class RouterConfig(private val webAdapter: WebAdapter) {

    @Bean
    fun routes(
        indexHandler: IndexHandler,
        emailLoginHandler: EmailLoginHandler
    ) = router {
        GET("/", indexHandler.adapt())
        GET("/login") { LoginForm }
        GET("/login/email") { EmailLoginForm }
        POST("/login/email", emailLoginHandler.adapt())
        GET("/login/token/{token}") { IndexRedirect }
    }

    private fun <T : Handler> T.adapt() =
        webAdapter.adapt(this::handle)

}