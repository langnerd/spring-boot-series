package com.langnerd.webfluxsecurity.model

import org.springframework.data.annotation.Id
import org.springframework.data.relational.core.mapping.Column
import org.springframework.data.relational.core.mapping.Table

@Table("users")
data class User(
    @Id
    val username: String,
    
    val password: String,

    @Column("user_role")
    val userRole: String,

    @Column("first_name")
    val firstName: String,

    @Column("last_name")
    val lastName: String,

    val email: String,

    val avatar: String? = null,

    val secret: String? = null,

    @Column("account_enabled")
    val accountEnabled: Boolean,

    @Column("account_expired")
    val accountExpired: Boolean,

    @Column("account_locked")
    val accountLocked: Boolean,

    @Column("credentials_expired")
    val credentialsExpired: Boolean,

    @Column("use_2fa")
    val use2FA: Boolean
)
