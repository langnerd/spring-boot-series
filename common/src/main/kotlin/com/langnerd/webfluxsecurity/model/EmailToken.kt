package com.langnerd.webfluxsecurity.model

import org.springframework.data.annotation.Id
import org.springframework.data.annotation.Version
import org.springframework.data.relational.core.mapping.Column
import org.springframework.data.relational.core.mapping.Table
import java.time.Instant

@Table("email_tokens")
class EmailToken(
    @Id
    val token: String,

    val email: String,

    val issued: Instant,

    @Column("token_expired")
    val expired: Boolean,

    @Version
    var version: Int? = null
)