package com.langnerd.webfluxsecurity.filter

import com.langnerd.webfluxsecurity.Logging.TraceIdContextKey
import com.langnerd.webfluxsecurity.dto.TraceId
import org.slf4j.MDC
import org.springframework.stereotype.Component
import org.springframework.web.server.ServerWebExchange
import org.springframework.web.server.WebFilter
import org.springframework.web.server.WebFilterChain
import reactor.core.publisher.Mono

@Component
class TraceIdFilter : WebFilter {

    companion object {
        const val TraceIdHeaderName = "X-Trace-Id"
    }

    override fun filter(exchange: ServerWebExchange, chain: WebFilterChain): Mono<Void> {
        val filteredExchange = if (exchange.request.headers.containsKey(TraceIdHeaderName)) exchange else {
            val traceId = MDC.get(TraceIdContextKey) ?: newTraceId()
            val req = exchange.request.mutate().header(TraceIdHeaderName, traceId).build()
            exchange.mutate().request(req).build()
        }
        return chain.filter(filteredExchange)
    }

    private fun newTraceId(): String {
        val traceId = TraceId.newInstance().value
        MDC.put(TraceIdContextKey, traceId)
        return traceId
    }
}