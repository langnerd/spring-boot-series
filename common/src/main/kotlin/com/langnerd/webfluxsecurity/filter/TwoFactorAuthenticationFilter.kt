package com.langnerd.webfluxsecurity.filter

import com.langnerd.webfluxsecurity.dto.UsernamePasswordPrincipal
import org.springframework.security.core.Authentication
import org.springframework.security.web.server.DefaultServerRedirectStrategy
import org.springframework.security.web.server.WebFilterExchange
import org.springframework.security.web.server.authentication.RedirectServerAuthenticationSuccessHandler
import reactor.core.publisher.Mono
import java.net.URI

class TwoFactorAuthenticationFilter : RedirectServerAuthenticationSuccessHandler() {

    companion object {
        private val OneTimeCodeUri = URI.create("/login/code")
    }

    private val redirectStrategy = DefaultServerRedirectStrategy()

    init {
        super.setRedirectStrategy(redirectStrategy)
    }

    override fun onAuthenticationSuccess(
        webFilterExchange: WebFilterExchange,
        authentication: Authentication
    ): Mono<Void> =
        when (val principal = authentication.principal) {
            is UsernamePasswordPrincipal ->
                if (principal.use2FA) redirectStrategy.sendRedirect(webFilterExchange.exchange, OneTimeCodeUri)
                else super.onAuthenticationSuccess(webFilterExchange, authentication)
            else -> super.onAuthenticationSuccess(webFilterExchange, authentication)
        }
}