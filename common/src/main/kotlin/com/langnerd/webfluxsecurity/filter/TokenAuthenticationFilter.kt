package com.langnerd.webfluxsecurity.filter

import com.langnerd.webfluxsecurity.dto.Email
import com.langnerd.webfluxsecurity.dto.EmailToken
import com.langnerd.webfluxsecurity.dto.UsernamePasswordPrincipal
import com.langnerd.webfluxsecurity.service.EmailTokenService
import com.langnerd.webfluxsecurity.service.EmailTokenService.EmailTokenValidationResult.*
import org.springframework.http.server.reactive.ServerHttpRequest
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.context.SecurityContextImpl
import org.springframework.security.web.server.context.WebSessionServerSecurityContextRepository
import org.springframework.web.server.ServerWebExchange
import org.springframework.web.server.WebFilter
import org.springframework.web.server.WebFilterChain
import reactor.core.publisher.Mono

class TokenAuthenticationFilter(
    private val emailTokenService: EmailTokenService,
    private val contextRepository: WebSessionServerSecurityContextRepository
) : WebFilter {

    companion object {
        const val FilterPath = "/login/token"
    }

    override fun filter(exchange: ServerWebExchange, chain: WebFilterChain): Mono<Void> {
        return if (exchange.request.path.value().startsWith(FilterPath)) {
            emailTokenService.validate(exchange.request.toToken()).flatMap { result ->
                when (result) {
                    is TokenExpired, TokenNotFound -> {
                        chain.filter(exchange)
                    }
                    is Success -> {
                        val principal = result.email.toPrincipal()
                        val authentication = UsernamePasswordAuthenticationToken(
                            principal, principal.password,
                            principal.authorities
                        )
                        val securityContext = SecurityContextImpl(authentication)
                        contextRepository.save(exchange, securityContext).then(chain.filter(exchange))
                    }
                }
            }
        } else chain.filter(exchange)
    }

    private fun ServerHttpRequest.toToken(): EmailToken =
        EmailToken(this.path.elements().last().value())

    private fun Email.toPrincipal(): UsernamePasswordPrincipal =
        UsernamePasswordPrincipal(
            username = "",
            password = "",
            role = "ROLE_USER",
            accountExpired = false,
            accountLocked = false,
            credentialsExpired = false,
            enabled = true,
            firstName = "",
            lastName = "",
            email = this.value,
            use2FA = false
        )
}
