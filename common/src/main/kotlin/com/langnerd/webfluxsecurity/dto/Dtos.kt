package com.langnerd.webfluxsecurity.dto

import com.bastiaanjansen.otp.SecretGenerator
import java.util.*

@JvmInline
value class Email(val value: String)

@JvmInline
value class EmailToken(val value: String)

@JvmInline
value class OneTimeCode(val value: Long)

@JvmInline
value class UserSecret(val value: String) {
    companion object {
        fun newInstance(): UserSecret =
            UserSecret(String(SecretGenerator.generate()))
    }
}

@JvmInline
value class TraceId(val value: String) {
    companion object {
        fun newInstance(): TraceId =
            TraceId(UUID.randomUUID().toString())
    }
}

data class UserDetails(
    val firstName: String,
    val lastName: String,
    val email: String,
    val use2FA: Boolean = false,
    val username: String? = null,
    val userRole: String? = null,
    val avatar: String? = null,
    val secret: String? = null
) {

    companion object {
        const val AdminRole = "ADMIN"
    }

    val name: String = "$firstName $lastName"

    val isAdmin: Boolean = userRole == AdminRole
}