package com.langnerd.webfluxsecurity.dto

import com.langnerd.webfluxsecurity.config.properties.OneTimePasswordProperties
import com.langnerd.webfluxsecurity.dto.Response.*
import com.langnerd.webfluxsecurity.service.OneTimePasswordManager
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.web.reactive.function.server.ServerResponse
import reactor.core.publisher.Mono
import reactor.kotlin.core.publisher.toMono
import java.net.URI

sealed interface Response {

    companion object {
        val IndexRedirect: Mono<ServerResponse> = Redirect("/").toWeb().toMono()
        val LoginForm: Mono<ServerResponse> = HtmlTemplate("login").toWeb().toMono()
        val EmailLoginForm: Mono<ServerResponse> = HtmlTemplate("login-email").toWeb().toMono()
        val AvatarChangeForm: Mono<ServerResponse> = HtmlTemplate("change-avatar").toWeb().toMono()
        val TwoFactorCodeForm: Mono<ServerResponse> = HtmlTemplate("2fa-code").toWeb().toMono()
    }

    @JvmInline
    value class Success(val value: Any) : Response

    @JvmInline
    value class BadRequest(val message: String) : Response

    @JvmInline
    value class Unauthorized(val message: String) : Response

    data class HtmlTemplate(val filename: String, val model: Map<String, Any> = emptyMap()) : Response

    data class Redirect(val path: String) : Response {
        companion object {
            val Home = Redirect("/")
            val Login = Redirect("/login")
        }
    }

    object View {
        fun index(userDetails: UserDetails, enable2FA: Boolean): HtmlTemplate =
            HtmlTemplate(
                "index", mapOf(
                    "user" to userDetails,
                    "disable2FAWarning" to !enable2FA,
                    "enable2FA" to (enable2FA && !userDetails.use2FA)
                )
            )

        fun changeEmail(userDetails: UserDetails): HtmlTemplate =
            HtmlTemplate("change-email", mapOf("user" to userDetails))

        fun userOverview(
            loggedInUser: UserDetails,
            enable2FA: Boolean,
            vararg userDetails: UserDetails
        ): HtmlTemplate =
            HtmlTemplate(
                "user-overview", mapOf(
                    "users" to userDetails,
                    "disable2FAWarning" to !enable2FA,
                    "enable2FA" to (enable2FA && !loggedInUser.use2FA),
                    "user" to loggedInUser
                )
            )

        fun loginEmailSent(email: Email): HtmlTemplate =
            HtmlTemplate("login-email-sent", mapOf("email" to email.value))

        fun twoFactorSetup(
            userDetails: UserDetails,
            oneTimePasswordProperties: OneTimePasswordProperties
        ): HtmlTemplate =
            HtmlTemplate(
                "2fa-setup",
                mapOf(
                    "user" to userDetails,
                    "disable2FAWarning" to true,
                    "qrCode" to OneTimePasswordManager.from(
                        Email(userDetails.email),
                        UserSecret(userDetails.secret!!),   // The secret must have been set at this point!
                        oneTimePasswordProperties
                    ).encodedChartUrl()
                )
            )

        fun twoFactorSetupConfirm(userDetails: UserDetails, isError: Boolean = false): HtmlTemplate =
            HtmlTemplate(
                "2fa-setup-confirm",
                mapOf("user" to userDetails, "disable2FAWarning" to true, "error" to isError)
            )
    }
}


fun Response.toWeb(): Mono<ServerResponse> =
    when (this) {
        is Success -> ServerResponse.ok().bodyValue(this.value)
        is BadRequest -> ServerResponse.badRequest().bodyValue(this.message)
        is HtmlTemplate -> ServerResponse.ok().contentType(MediaType.TEXT_HTML)
            .render(this.filename, this.model)
        is Redirect -> ServerResponse.temporaryRedirect(URI.create(this.path)).build()
        is Unauthorized -> ServerResponse.status(HttpStatus.UNAUTHORIZED).bodyValue(this.message)
    }
