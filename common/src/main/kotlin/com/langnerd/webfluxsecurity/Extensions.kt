package com.langnerd.webfluxsecurity

import com.langnerd.webfluxsecurity.dao.UserRepository
import com.langnerd.webfluxsecurity.dto.Email
import com.langnerd.webfluxsecurity.dto.OneTimeCode
import com.langnerd.webfluxsecurity.dto.UserDetails
import com.langnerd.webfluxsecurity.dto.UsernamePasswordPrincipal
import com.langnerd.webfluxsecurity.model.User
import com.langnerd.webfluxsecurity.service.UserDetailService
import org.springframework.security.core.context.ReactiveSecurityContextHolder
import org.springframework.web.reactive.function.server.ServerRequest
import reactor.core.publisher.Mono
import reactor.kotlin.core.publisher.switchIfEmpty
import java.net.URI

fun User.toUserDetails() =
    UserDetails(
        this.firstName,
        this.lastName,
        this.email,
        this.use2FA,
        this.username,
        this.userRole,
        this.avatar,
        this.secret
    )

fun User.toUsernamePasswordPrincipal(): UsernamePasswordPrincipal =
    UsernamePasswordPrincipal(
        this.username,
        this.password,
        this.userRole,
        this.accountExpired,
        this.accountLocked,
        this.credentialsExpired,
        this.accountEnabled,
        this.firstName,
        this.lastName,
        this.email,
        this.use2FA,
        this.avatar
    )

fun UserRepository.withCurrentUser(): Mono<User> =
    withAuthentication().flatMap { this.findByUsername(it) }

fun UserDetailService.withCurrentUser(): Mono<UserDetails> =
    withAuthentication().flatMap { this.findByUsername(it) }

fun ServerRequest.withUserEmail(): Mono<Email> =
    this.formData().map { Email(it["email"]!!.first()) }

fun ServerRequest.withUserAvatar(): Mono<URI> =
    this.formData().map { URI.create(it["avatar"]!!.first()) }

fun ServerRequest.withOneTimeCode(): Mono<OneTimeCode> =
    this.formData().map { OneTimeCode(it["code"]!!.first().toLong()) }
        .onErrorResume { Mono.empty() }

private fun withAuthentication(): Mono<String> =
    ReactiveSecurityContextHolder.getContext()
        .map { it.authentication.name }
        .switchIfEmpty { Mono.error(IllegalStateException("Invalid login")) }