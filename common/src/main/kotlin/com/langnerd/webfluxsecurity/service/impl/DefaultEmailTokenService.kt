package com.langnerd.webfluxsecurity.service.impl

import com.langnerd.webfluxsecurity.config.properties.EmailTokenProperties
import com.langnerd.webfluxsecurity.dao.EmailTokenRepository
import com.langnerd.webfluxsecurity.dto.Email
import com.langnerd.webfluxsecurity.model.EmailToken
import com.langnerd.webfluxsecurity.service.EmailSender
import com.langnerd.webfluxsecurity.service.EmailTokenService
import com.langnerd.webfluxsecurity.service.EmailTokenService.EmailTokenValidationResult
import com.langnerd.webfluxsecurity.service.EmailTokenService.EmailTokenValidationResult.TokenExpired
import org.springframework.security.crypto.codec.Hex
import org.springframework.stereotype.Service
import reactor.core.publisher.Mono
import reactor.kotlin.core.publisher.toMono
import java.security.SecureRandom
import java.time.Duration
import java.time.Instant
import com.langnerd.webfluxsecurity.dto.EmailToken as EmailTokenDto

@Service
class DefaultEmailTokenService(
    private val emailTokenProperties: EmailTokenProperties,
    private val emailTokenRepository: EmailTokenRepository,
    private val emailSender: EmailSender
) : EmailTokenService {

    companion object {
        const val TokenByteSize = 16
        val TokenExpiredResponse = TokenExpired.toMono()
    }

    private val tokenGenerator = SecureRandom()

    override fun createAndSend(email: Email): Mono<EmailTokenDto> =
        emailTokenRepository.save(
            EmailToken(
                nextToken(),
                email.value,
                Instant.now(),
                false
            )
        )
            .map { emailToken -> EmailTokenDto(emailToken.token) }
            .doOnSuccess { emailSender.send(email, it) }


    override fun validate(emailToken: EmailTokenDto): Mono<EmailTokenValidationResult> =
        emailTokenRepository.findById(emailToken.value).flatMap { foundToken ->
            if (foundToken.expired || isTooOld(foundToken.issued)) TokenExpiredResponse
            else {
                emailTokenRepository.expire(foundToken.token)
                    .map { EmailTokenValidationResult.Success(Email(foundToken.email)) }
            }
        }.defaultIfEmpty(EmailTokenValidationResult.TokenNotFound)

    private fun isTooOld(issued: Instant): Boolean =
        Duration.between(issued, Instant.now()) >= emailTokenProperties.expiry

    private fun nextToken(): String {
        val bytes = ByteArray(TokenByteSize)
        tokenGenerator.nextBytes(bytes)
        return String(Hex.encode(bytes))
    }
}