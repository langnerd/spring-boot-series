package com.langnerd.webfluxsecurity.service

import com.langnerd.webfluxsecurity.dto.Email
import com.langnerd.webfluxsecurity.dto.EmailToken
import reactor.core.publisher.Mono

interface EmailSender {

    fun send(to: Email, emailToken: EmailToken): Mono<Unit>
}