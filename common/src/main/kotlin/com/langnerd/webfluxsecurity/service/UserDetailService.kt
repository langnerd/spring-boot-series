package com.langnerd.webfluxsecurity.service

import com.langnerd.webfluxsecurity.dto.Email
import com.langnerd.webfluxsecurity.dto.OneTimeCode
import com.langnerd.webfluxsecurity.dto.UserDetails
import reactor.core.publisher.Mono
import java.net.URI

interface UserDetailService {

    fun findAll(): Mono<List<UserDetails>>

    fun findByUsername(username: String): Mono<UserDetails>

    fun setAvatar(uri: URI): Mono<UserDetails>

    fun setEmail(email: Email): Mono<UserDetails>

    fun enable2FA(code: OneTimeCode): Mono<OneTimeCodeValidationResult>

    fun isValid(code: OneTimeCode): Mono<OneTimeCodeValidationResult>

    fun setSecret(): Mono<UserDetails>

    sealed interface OneTimeCodeValidationResult {
        @JvmInline
        value class Success(val userDetails: UserDetails) : OneTimeCodeValidationResult
        object CodeInvalid : OneTimeCodeValidationResult
    }
}
