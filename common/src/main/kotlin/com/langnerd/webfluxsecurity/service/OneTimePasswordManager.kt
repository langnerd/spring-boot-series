package com.langnerd.webfluxsecurity.service

import com.bastiaanjansen.otp.TOTPGenerator
import com.langnerd.webfluxsecurity.config.properties.OneTimePasswordProperties
import com.langnerd.webfluxsecurity.dto.Email
import com.langnerd.webfluxsecurity.dto.OneTimeCode
import com.langnerd.webfluxsecurity.dto.UserSecret
import java.net.URLEncoder

class OneTimePasswordManager private constructor(
    private val email: Email,
    private val secret: UserSecret,
    private val properties: OneTimePasswordProperties
) {

    companion object {
        fun from(email: Email, secret: UserSecret, properties: OneTimePasswordProperties): OneTimePasswordManager =
            OneTimePasswordManager(email, secret, properties)
    }

    fun isValid(code: OneTimeCode): Boolean =
        totp(email).verify(code.value.toString())

    fun encodedChartUrl(): String =
        "${properties.chartUrl}${
            URLEncoder.encode(
                properties.baseUrl.expand(email.value, secret.value).toString(), Charsets.UTF_8
            )
        }"

    private fun totp(email: Email): TOTPGenerator =
        TOTPGenerator.Builder.fromOTPAuthURI(properties.baseUrl.expand(email.value, secret.value))
}