package com.langnerd.webfluxsecurity.service.impl

import com.langnerd.webfluxsecurity.dao.UserRepository
import com.langnerd.webfluxsecurity.toUsernamePasswordPrincipal
import org.springframework.security.core.userdetails.ReactiveUserDetailsService
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.stereotype.Service
import reactor.core.publisher.Mono

@Service
class SpringUserDetailsService(private val userRepository: UserRepository) : ReactiveUserDetailsService {

    override fun findByUsername(username: String): Mono<UserDetails> =
        userRepository.findByUsername(username).map { it.toUsernamePasswordPrincipal() }
}