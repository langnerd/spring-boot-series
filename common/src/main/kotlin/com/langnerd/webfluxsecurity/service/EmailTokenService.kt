package com.langnerd.webfluxsecurity.service

import com.langnerd.webfluxsecurity.dto.Email
import com.langnerd.webfluxsecurity.dto.EmailToken
import reactor.core.publisher.Mono

interface EmailTokenService {

    fun createAndSend(email: Email): Mono<EmailToken>

    fun validate(emailToken: EmailToken): Mono<EmailTokenValidationResult>

    sealed interface EmailTokenValidationResult {
        object TokenExpired : EmailTokenValidationResult
        object TokenNotFound : EmailTokenValidationResult
        @JvmInline value class Success(val email: Email) : EmailTokenValidationResult
    }
}
