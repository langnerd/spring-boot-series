package com.langnerd.webfluxsecurity.service.impl

import com.langnerd.webfluxsecurity.config.properties.EmailTokenProperties
import com.langnerd.webfluxsecurity.dto.Email
import com.langnerd.webfluxsecurity.dto.EmailToken
import com.langnerd.webfluxsecurity.service.EmailSender
import org.springframework.mail.MailSender
import org.springframework.mail.SimpleMailMessage
import org.springframework.stereotype.Service
import reactor.core.publisher.Mono
import reactor.kotlin.core.publisher.toMono

@Service
class DefaultEmailSender(
    private val mailSender: MailSender,
    private val emailTokenProperties: EmailTokenProperties
) : EmailSender {

    override fun send(to: Email, emailToken: EmailToken): Mono<Unit> {
        val message = SimpleMailMessage()
        message.setFrom(emailTokenProperties.from)
        message.setTo(to.value)
        message.setSubject("Since you asked to sign in")
        message.setText("Please follow this link to sign in:\n\n ${emailTokenProperties.url.expand(emailToken.value)}")
        return mailSender.send(message).toMono()
    }
}