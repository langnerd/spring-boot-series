package com.langnerd.webfluxsecurity.service.impl

import com.langnerd.webfluxsecurity.config.properties.OneTimePasswordProperties
import com.langnerd.webfluxsecurity.dao.UserRepository
import com.langnerd.webfluxsecurity.dto.Email
import com.langnerd.webfluxsecurity.dto.OneTimeCode
import com.langnerd.webfluxsecurity.dto.UserDetails
import com.langnerd.webfluxsecurity.dto.UserSecret
import com.langnerd.webfluxsecurity.model.User
import com.langnerd.webfluxsecurity.service.OneTimePasswordManager
import com.langnerd.webfluxsecurity.service.UserDetailService
import com.langnerd.webfluxsecurity.service.UserDetailService.OneTimeCodeValidationResult
import com.langnerd.webfluxsecurity.service.UserDetailService.OneTimeCodeValidationResult.CodeInvalid
import com.langnerd.webfluxsecurity.service.UserDetailService.OneTimeCodeValidationResult.Success
import com.langnerd.webfluxsecurity.toUserDetails
import com.langnerd.webfluxsecurity.withCurrentUser
import org.springframework.stereotype.Service
import reactor.core.publisher.Mono
import reactor.kotlin.core.publisher.toMono
import java.net.URI
import java.net.URLEncoder
import java.util.Comparator.comparing

@Service
class DefaultUserDetailsService(
    private val userRepository: UserRepository,
    private val oneTimePasswordProperties: OneTimePasswordProperties
) :
    UserDetailService {

    override fun findAll(): Mono<List<UserDetails>> =
        userRepository.findAll()
            .sort(comparing(User::username).thenComparing(User::email))
            .map { it.toUserDetails() }
            .collectList()

    override fun findByUsername(username: String): Mono<UserDetails> =
        userRepository.findByUsername(username).map { it.toUserDetails() }

    override fun setAvatar(uri: URI): Mono<UserDetails> =
        userRepository.withCurrentUser().flatMap { user ->
            userRepository.save(user.copy(avatar = URLEncoder.encode(uri.toString(), Charsets.UTF_8)))
        }.map { it.toUserDetails() }

    override fun setEmail(email: Email): Mono<UserDetails> =
        userRepository.withCurrentUser().flatMap { user ->
            userRepository.save(user.copy(email = email.value))
        }.map { it.toUserDetails() }

    override fun enable2FA(code: OneTimeCode): Mono<OneTimeCodeValidationResult> =
        userRepository.withCurrentUser()
            .flatMap { user ->
                if (isValid(user.email, user.secret, code)) {
                    userRepository
                        .save(user.copy(use2FA = true))
                        .map { Success(user.toUserDetails()) }
                } else CodeInvalid.toMono()
            }

    override fun isValid(code: OneTimeCode): Mono<OneTimeCodeValidationResult> =
        withCurrentUser()
            .map { userDetails ->
                if (isValid(userDetails.email, userDetails.secret, code)) Success(userDetails) else CodeInvalid
            }

    override fun setSecret(): Mono<UserDetails> =
        userRepository.withCurrentUser().flatMap { user ->
            userRepository.save(user.copy(secret = UserSecret.newInstance().value))
        }.map { it.toUserDetails() }

    private fun isValid(email: String, secret: String?, code: OneTimeCode): Boolean =
        OneTimePasswordManager.from(
            Email(email),
            secret?.let(::UserSecret) ?: UserSecret.newInstance(),
            oneTimePasswordProperties).isValid(code)


}