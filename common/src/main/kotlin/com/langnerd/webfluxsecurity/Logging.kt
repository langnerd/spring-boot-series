package com.langnerd.webfluxsecurity

import com.langnerd.webfluxsecurity.dto.TraceId
import org.slf4j.MDC
import reactor.core.publisher.Mono

object Logging {

    const val TraceIdContextKey = "traceIdContext"

    private const val TraceIdMDCKey = "traceId"

    fun <T> Mono<T>.logOnNext(f: (T) -> Unit): Mono<T> = this.withTraceId().doOnNext(f)
    fun <T> Mono<T>.logOnSuccess(f: (T) -> Unit): Mono<T> = this.withTraceId().doOnSuccess(f)
    fun <T> Mono<T>.logOnError(f: (Throwable) -> Unit): Mono<T> = this.withTraceId().doOnError(f)

    private fun <T> Mono<T>.withTraceId(): Mono<T> = this.doOnEach { signal ->
        signal.contextView.getOrEmpty<TraceId>(TraceIdContextKey).ifPresent { traceId ->
            MDC.put(TraceIdMDCKey, traceId.value)
        }
    }
}