package com.langnerd.webfluxsecurity.config.properties

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.ConstructorBinding
import org.springframework.web.util.UriTemplate
import java.time.Duration

@ConstructorBinding
@ConfigurationProperties(prefix = "app.security.email-token")
data class EmailTokenProperties(val expiry: Duration, val from: String, val url: UriTemplate)