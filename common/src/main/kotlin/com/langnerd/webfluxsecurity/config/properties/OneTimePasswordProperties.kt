package com.langnerd.webfluxsecurity.config.properties

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.ConstructorBinding
import org.springframework.web.util.UriTemplate

@ConstructorBinding
@ConfigurationProperties(prefix = "app.security.totp")
data class OneTimePasswordProperties(
    val enabled: Boolean,
    val baseUrl: UriTemplate,
    val chartUrl: UriTemplate
)
