package com.langnerd.webfluxsecurity.config.properties

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.ConstructorBinding

@ConstructorBinding
@ConfigurationProperties(prefix = "app.email")
data class EmailProperties(val host: String, val port: Int, val username: String, val password: String)
