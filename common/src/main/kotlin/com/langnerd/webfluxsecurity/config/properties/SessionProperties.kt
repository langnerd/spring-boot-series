package com.langnerd.webfluxsecurity.config.properties

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.ConstructorBinding
import java.time.Duration

@ConstructorBinding
@ConfigurationProperties(prefix = "app.security.session")
data class SessionProperties(val timeout: Duration)
