package com.langnerd.webfluxsecurity.config.properties

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.ConstructorBinding
import java.net.URI

@ConstructorBinding
@ConfigurationProperties(prefix = "app.security.avatar")
data class AvatarProperties(val whitelistedDomains: List<URI>)
