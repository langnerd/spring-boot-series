package com.langnerd.webfluxsecurity.config

import com.langnerd.webfluxsecurity.config.properties.SessionProperties
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.InjectionPoint
import org.springframework.beans.factory.config.BeanDefinition
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Scope
import org.springframework.session.ReactiveMapSessionRepository
import java.util.concurrent.ConcurrentHashMap

@Configuration
class CommonConfig {

    @Bean
    @Scope(BeanDefinition.SCOPE_PROTOTYPE)
    fun logger(ip: InjectionPoint): Logger = LoggerFactory.getLogger(ip.member.declaringClass)

    @Bean
    fun sessionRepository(sessionProperties: SessionProperties): ReactiveMapSessionRepository { // Helps prevent authentication failures by reliably expiring the user session: https://owasp.org/Top10/A07_2021-Identification_and_Authentication_Failures
        val sessionRepository = ReactiveMapSessionRepository(ConcurrentHashMap())
        sessionRepository.setDefaultMaxInactiveInterval(sessionProperties.timeout.seconds.toInt())
        return sessionRepository
    }
}