package com.langnerd.webfluxsecurity.dao

import com.langnerd.webfluxsecurity.model.User
import org.springframework.data.r2dbc.repository.Query
import org.springframework.data.repository.reactive.ReactiveCrudRepository
import reactor.core.publisher.Mono

interface UserRepository : ReactiveCrudRepository<User, String> {
    
    @Query("SELECT * FROM users WHERE LOWER(username) = LOWER(:username)")
    fun findByUsername(username: String): Mono<User>
}