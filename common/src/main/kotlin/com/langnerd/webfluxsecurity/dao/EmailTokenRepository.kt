package com.langnerd.webfluxsecurity.dao

import com.langnerd.webfluxsecurity.model.EmailToken
import org.springframework.data.r2dbc.repository.Modifying
import org.springframework.data.r2dbc.repository.Query
import org.springframework.data.repository.reactive.ReactiveCrudRepository
import reactor.core.publisher.Mono

interface EmailTokenRepository : ReactiveCrudRepository<EmailToken, String> {

    @Modifying
    @Query("UPDATE email_tokens SET token_expired = true WHERE token = :token")
    fun expire(token: String): Mono<Boolean>
}