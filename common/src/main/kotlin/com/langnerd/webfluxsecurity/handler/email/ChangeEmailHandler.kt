package com.langnerd.webfluxsecurity.handler.email

import com.langnerd.webfluxsecurity.Logging.logOnNext
import com.langnerd.webfluxsecurity.Logging.logOnSuccess
import com.langnerd.webfluxsecurity.config.properties.OneTimePasswordProperties
import com.langnerd.webfluxsecurity.dto.Response
import com.langnerd.webfluxsecurity.dto.Response.View.userOverview
import com.langnerd.webfluxsecurity.handler.Handler
import com.langnerd.webfluxsecurity.service.UserDetailService
import com.langnerd.webfluxsecurity.withUserEmail
import org.slf4j.Logger
import org.springframework.stereotype.Component
import org.springframework.web.reactive.function.server.ServerRequest
import reactor.core.publisher.Mono

@Component
class ChangeEmailHandler(
    private val userDetailsService: UserDetailService,
    private val oneTimePasswordProperties: OneTimePasswordProperties,
    private val logger: Logger
) : Handler {

    override fun handle(req: ServerRequest): Mono<Response> =
        req.withUserEmail()
            .logOnNext { logger.debug("Changing email to ${it.value}") }
            .flatMap(userDetailsService::setEmail)
            .logOnSuccess { logger.debug("Changed email for $it") }
            .map { userDetails -> userOverview(userDetails, oneTimePasswordProperties.enabled)}
}