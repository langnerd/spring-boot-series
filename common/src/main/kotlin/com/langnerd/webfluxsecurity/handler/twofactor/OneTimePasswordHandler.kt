package com.langnerd.webfluxsecurity.handler.twofactor

import com.langnerd.webfluxsecurity.Logging.logOnSuccess
import com.langnerd.webfluxsecurity.dto.Response
import com.langnerd.webfluxsecurity.dto.Response.Redirect.Companion.Home
import com.langnerd.webfluxsecurity.dto.Response.Redirect.Companion.Login
import com.langnerd.webfluxsecurity.handler.Handler
import com.langnerd.webfluxsecurity.service.UserDetailService
import com.langnerd.webfluxsecurity.service.UserDetailService.OneTimeCodeValidationResult.CodeInvalid
import com.langnerd.webfluxsecurity.service.UserDetailService.OneTimeCodeValidationResult.Success
import com.langnerd.webfluxsecurity.withOneTimeCode
import org.slf4j.Logger
import org.springframework.stereotype.Component
import org.springframework.web.reactive.function.server.ServerRequest
import reactor.core.publisher.Mono
import reactor.kotlin.core.publisher.switchIfEmpty
import reactor.kotlin.core.publisher.toMono

@Component
class OneTimePasswordHandler(
    private val userDetailService: UserDetailService,
    private val logger: Logger
) : Handler {

    override fun handle(req: ServerRequest): Mono<Response> =
        req.withOneTimeCode().flatMap<Response> { code ->
            userDetailService.isValid(code).flatMap { result ->
                when (result) {
                    is Success -> Home.toMono()
                    is CodeInvalid -> Login.toMono()
                }.logOnSuccess { logger.debug("Code $code evaluated as $result") }
            }
        }.switchIfEmpty { Login.toMono() }
}
