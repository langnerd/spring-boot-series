package com.langnerd.webfluxsecurity.handler

import com.langnerd.webfluxsecurity.dto.Response
import org.springframework.web.reactive.function.server.ServerRequest
import reactor.core.publisher.Mono

interface Handler {

    fun handle(req: ServerRequest): Mono<Response>
}