package com.langnerd.webfluxsecurity.handler.email

import com.langnerd.webfluxsecurity.Logging.logOnSuccess
import com.langnerd.webfluxsecurity.dto.Response
import com.langnerd.webfluxsecurity.dto.Response.View.changeEmail
import com.langnerd.webfluxsecurity.handler.Handler
import com.langnerd.webfluxsecurity.service.UserDetailService
import com.langnerd.webfluxsecurity.withCurrentUser
import org.slf4j.Logger
import org.springframework.stereotype.Component
import org.springframework.web.reactive.function.server.ServerRequest
import reactor.core.publisher.Mono

@Component
class ChangeEmailFormHandler(
    private val userDetailsService: UserDetailService,
    private val logger: Logger
) : Handler {

    override fun handle(req: ServerRequest): Mono<Response> =
        userDetailsService.withCurrentUser()
            .logOnSuccess { logger.debug("Displaying ChangeEmail view for $it") }
            .map(::changeEmail)
}