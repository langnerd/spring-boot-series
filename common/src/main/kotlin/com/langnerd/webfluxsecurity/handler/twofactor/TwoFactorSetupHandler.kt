package com.langnerd.webfluxsecurity.handler.twofactor

import com.langnerd.webfluxsecurity.Logging.logOnSuccess
import com.langnerd.webfluxsecurity.dto.Response
import com.langnerd.webfluxsecurity.dto.Response.Redirect.Companion.Home
import com.langnerd.webfluxsecurity.dto.Response.View.twoFactorSetupConfirm
import com.langnerd.webfluxsecurity.dto.UserDetails
import com.langnerd.webfluxsecurity.handler.Handler
import com.langnerd.webfluxsecurity.service.UserDetailService
import com.langnerd.webfluxsecurity.service.UserDetailService.OneTimeCodeValidationResult.CodeInvalid
import com.langnerd.webfluxsecurity.service.UserDetailService.OneTimeCodeValidationResult.Success
import com.langnerd.webfluxsecurity.withCurrentUser
import com.langnerd.webfluxsecurity.withOneTimeCode
import org.slf4j.Logger
import org.springframework.stereotype.Component
import org.springframework.web.reactive.function.server.ServerRequest
import reactor.core.publisher.Mono
import reactor.kotlin.core.publisher.switchIfEmpty
import reactor.kotlin.core.publisher.toMono

@Component
class TwoFactorSetupHandler(
    private val userDetailService: UserDetailService,
    private val logger: Logger
) : Handler {

    override fun handle(req: ServerRequest): Mono<Response> =
        req.withOneTimeCode()
            .flatMap { code ->
                userDetailService.enable2FA(code).flatMap { result ->
                    when (result) {
                        is Success ->
                            Home.toMono()
                                .logOnSuccess { logger.debug("2FA enabled for ${result.userDetails}") }
                        is CodeInvalid ->
                            userDetailService
                                .withCurrentUser().map(::invalidCode)
                                .logOnSuccess { logger.debug("OTP code invalid: $code") }
                    }
                }
            }.switchIfEmpty { userDetailService.withCurrentUser().map(::invalidCode) }

    private fun invalidCode(userDetails: UserDetails) = twoFactorSetupConfirm(userDetails, true)

}