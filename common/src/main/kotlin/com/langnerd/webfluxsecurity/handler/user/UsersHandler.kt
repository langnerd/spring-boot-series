package com.langnerd.webfluxsecurity.handler.user

import com.langnerd.webfluxsecurity.Logging.logOnSuccess
import com.langnerd.webfluxsecurity.config.properties.OneTimePasswordProperties
import com.langnerd.webfluxsecurity.dto.Response
import com.langnerd.webfluxsecurity.dto.Response.View.userOverview
import com.langnerd.webfluxsecurity.handler.Handler
import com.langnerd.webfluxsecurity.service.UserDetailService
import com.langnerd.webfluxsecurity.withCurrentUser
import org.slf4j.Logger
import org.springframework.stereotype.Component
import org.springframework.web.reactive.function.server.ServerRequest
import reactor.core.publisher.Mono

@Component
class UsersHandler(
    private val userDetailsService: UserDetailService,
    private val oneTimePasswordProperties: OneTimePasswordProperties,
    private val logger: Logger
) : Handler {

    override fun handle(req: ServerRequest): Mono<Response> =
        userDetailsService
            .findAll()
            .logOnSuccess { logger.debug("Displaying details of all users!") }
            .flatMap { userDetails ->
                userDetailsService.withCurrentUser().map { currentUser ->
                    userOverview(currentUser, oneTimePasswordProperties.enabled, * userDetails.toTypedArray())
                }
            }

}