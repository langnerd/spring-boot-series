package com.langnerd.webfluxsecurity.handler.twofactor

import com.langnerd.webfluxsecurity.Logging.logOnSuccess
import com.langnerd.webfluxsecurity.config.properties.OneTimePasswordProperties
import com.langnerd.webfluxsecurity.dto.Response
import com.langnerd.webfluxsecurity.dto.Response.View.twoFactorSetup
import com.langnerd.webfluxsecurity.handler.Handler
import com.langnerd.webfluxsecurity.service.UserDetailService
import com.langnerd.webfluxsecurity.withCurrentUser
import org.slf4j.Logger
import org.springframework.stereotype.Component
import org.springframework.web.reactive.function.server.ServerRequest
import reactor.core.publisher.Mono

@Component
class TwoFactorSetupFormHandler(
    private val oneTimePasswordProperties: OneTimePasswordProperties,
    private val userDetailService: UserDetailService,
    private val logger: Logger
) : Handler {

    override fun handle(req: ServerRequest): Mono<Response> =
        userDetailService.setSecret()
            .logOnSuccess { logger.debug("Displaying TwoFactorSetup view for $it") }
            .map { userDetails -> twoFactorSetup(userDetails, oneTimePasswordProperties)}
}