package com.langnerd.webfluxsecurity.handler.user

import com.langnerd.webfluxsecurity.Logging.logOnSuccess
import com.langnerd.webfluxsecurity.config.properties.OneTimePasswordProperties
import com.langnerd.webfluxsecurity.dto.Response
import com.langnerd.webfluxsecurity.dto.Response.Unauthorized
import com.langnerd.webfluxsecurity.dto.Response.View.userOverview
import com.langnerd.webfluxsecurity.dto.UserDetails
import com.langnerd.webfluxsecurity.handler.Handler
import com.langnerd.webfluxsecurity.service.UserDetailService
import com.langnerd.webfluxsecurity.withCurrentUser
import org.slf4j.Logger
import org.springframework.stereotype.Component
import org.springframework.web.reactive.function.server.ServerRequest
import reactor.core.publisher.Mono
import reactor.kotlin.core.publisher.switchIfEmpty
import reactor.kotlin.core.publisher.toMono

@Component
class UserHandler(
    private val userDetailsService: UserDetailService,
    private val oneTimePasswordProperties: OneTimePasswordProperties,
    private val logger: Logger
) : Handler {

    companion object {
        const val UsernameVariable = "username"
        val unauthorizedResponse: Mono<Response> =
            Unauthorized("Unauthorized access detected. The incident has been reported to the admin!").toMono()
    }

    override fun handle(req: ServerRequest): Mono<Response> {
        // BAD QUERY 1: Fully open to an SQL injection, potentially returning ALL users from the database!
//        userRepository.findAllById(listOf(req.pathVariable(UsernameVariable))).collectList().map { users ->
//            HtmlTemplate("user-overview", mapOf("users" to users))
//        }

        // BAD QUERY 2: Still vulnerable, potentially returning ANY (single) user from the database!
//        userRepository.findByUsername(req.pathVariable(UsernameVariable)).map { user ->
//            HtmlTemplate("user-overview", mapOf("users" to listOf(user)))
//        }

        // CORRECT APPROACH: User input validation!
        // Handles SQL Injection: https://owasp.org/Top10/A03_2021-Injection
        return userDetailsService.findByUsername(req.pathVariable(UsernameVariable)).flatMap { userDetails ->
            hasAccess(userDetails).flatMap { accessAllowed ->
                if (accessAllowed) userDetailsService.withCurrentUser()
                    .map { currentUser -> userOverview(currentUser, oneTimePasswordProperties.enabled, userDetails) }
                else unauthorizedResponse.logOnSuccess {
                    logger.debug("Unauthorized access to $userDetails")
                }
            }
                .switchIfEmpty { unauthorizedResponse }
                .logOnSuccess { logger.debug("No user found by ${req.pathVariable(UsernameVariable)}") }
        }
    }

    private fun hasAccess(userDetails: UserDetails): Mono<Boolean> =
        userDetailsService.withCurrentUser().map { it.isAdmin || it == userDetails }
}