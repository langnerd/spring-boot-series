package com.langnerd.webfluxsecurity.handler.email

import com.langnerd.webfluxsecurity.Logging.logOnSuccess
import com.langnerd.webfluxsecurity.dto.Response
import com.langnerd.webfluxsecurity.dto.Response.View.loginEmailSent
import com.langnerd.webfluxsecurity.handler.Handler
import com.langnerd.webfluxsecurity.service.EmailTokenService
import com.langnerd.webfluxsecurity.withUserEmail
import org.slf4j.Logger
import org.springframework.stereotype.Component
import org.springframework.web.reactive.function.server.ServerRequest
import reactor.core.publisher.Mono

@Component
class EmailLoginHandler(
    private val emailTokenService: EmailTokenService,
    private val logger: Logger
) : Handler {

    override fun handle(req: ServerRequest): Mono<Response> =
        req.withUserEmail().flatMap { email ->
            emailTokenService.createAndSend(email)
                .logOnSuccess { logger.debug("Sent token $it to $email") }
                .map { loginEmailSent(email) }
        }
}