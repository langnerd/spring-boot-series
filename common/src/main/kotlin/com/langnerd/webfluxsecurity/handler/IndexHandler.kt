package com.langnerd.webfluxsecurity.handler

import com.langnerd.webfluxsecurity.Logging.logOnSuccess
import com.langnerd.webfluxsecurity.config.properties.OneTimePasswordProperties
import com.langnerd.webfluxsecurity.dto.Response
import com.langnerd.webfluxsecurity.dto.Response.Redirect
import com.langnerd.webfluxsecurity.dto.Response.View.index
import com.langnerd.webfluxsecurity.service.UserDetailService
import com.langnerd.webfluxsecurity.withCurrentUser
import org.slf4j.Logger
import org.springframework.stereotype.Component
import org.springframework.web.reactive.function.server.ServerRequest
import reactor.core.publisher.Mono
import reactor.kotlin.core.publisher.switchIfEmpty
import reactor.kotlin.core.publisher.toMono

@Component
class IndexHandler(
    private val userDetailService: UserDetailService,
    private val oneTimePasswordProperties: OneTimePasswordProperties,
    private val logger: Logger
) : Handler {

    companion object {
        private val RedirectToLoginPage: Mono<Response> = Redirect("login").toMono()
    }

    override fun handle(req: ServerRequest): Mono<Response> =
        userDetailService.withCurrentUser()
            .logOnSuccess { logger.debug("Displaying index page for $it") }
            .map<Response> { userDetails -> index(userDetails, oneTimePasswordProperties.enabled) }
            .onErrorResume { RedirectToLoginPage }
            .switchIfEmpty { RedirectToLoginPage }
}