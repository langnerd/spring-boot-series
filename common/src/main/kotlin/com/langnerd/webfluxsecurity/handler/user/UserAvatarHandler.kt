package com.langnerd.webfluxsecurity.handler.user

import com.langnerd.webfluxsecurity.Logging.logOnSuccess
import com.langnerd.webfluxsecurity.config.properties.AvatarProperties
import com.langnerd.webfluxsecurity.config.properties.OneTimePasswordProperties
import com.langnerd.webfluxsecurity.dto.Response
import com.langnerd.webfluxsecurity.dto.Response.HtmlTemplate
import com.langnerd.webfluxsecurity.dto.Response.View.userOverview
import com.langnerd.webfluxsecurity.handler.Handler
import com.langnerd.webfluxsecurity.service.UserDetailService
import com.langnerd.webfluxsecurity.withCurrentUser
import com.langnerd.webfluxsecurity.withUserAvatar
import org.slf4j.Logger
import org.springframework.stereotype.Component
import org.springframework.web.reactive.function.server.ServerRequest
import reactor.core.publisher.Mono
import java.net.URI
import java.net.URLEncoder
import java.nio.charset.StandardCharsets

@Component
class UserAvatarHandler(
    private val avatarProperties: AvatarProperties,
    private val userService: UserDetailService,
    private val oneTimePasswordProperties: OneTimePasswordProperties,
    private val logger: Logger
) : Handler {

    override fun handle(req: ServerRequest): Mono<Response> =
        req.withUserAvatar().flatMap { uri ->
            if (isWhitelistedDomain(uri)) {
                userService.setAvatar(uri)
                    .logOnSuccess { logger.debug("Set avatar for $it to: $uri") }
                    .map { userDetails -> userOverview(userDetails, oneTimePasswordProperties.enabled) }
            } else {
                userService.withCurrentUser()
                    .logOnSuccess { logger.warn("User $it tried to upload file from an illegal site: \"${encode(uri)}\"") }
                    .map {
                        HtmlTemplate(
                            "change-avatar",
                            mapOf("error" to "Invalid link. The incident has been reported.")
                        )
                    }
            }
        }

    private fun encode(uri: URI): String =
        URLEncoder.encode(uri.toString(), StandardCharsets.UTF_8)

    private fun isWhitelistedDomain(uri: URI): Boolean =
        avatarProperties.whitelistedDomains.any { uri.toString().startsWith(it.toString()) }
}