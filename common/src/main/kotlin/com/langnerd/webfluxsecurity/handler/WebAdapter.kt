package com.langnerd.webfluxsecurity.handler

import com.langnerd.webfluxsecurity.Logging.TraceIdContextKey
import com.langnerd.webfluxsecurity.Logging.logOnError
import com.langnerd.webfluxsecurity.Logging.logOnSuccess
import com.langnerd.webfluxsecurity.dto.Response
import com.langnerd.webfluxsecurity.dto.TraceId
import com.langnerd.webfluxsecurity.dto.toWeb
import com.langnerd.webfluxsecurity.filter.TraceIdFilter.Companion.TraceIdHeaderName
import org.slf4j.Logger
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Component
import org.springframework.web.reactive.function.server.ServerRequest
import org.springframework.web.reactive.function.server.ServerResponse
import reactor.core.publisher.Mono

@Component
class WebAdapter(private val logger: Logger) {

    companion object {
        val ServerError = ServerResponse
            .status(HttpStatus.INTERNAL_SERVER_ERROR)
            .bodyValue("Oops, something went wrong.")
    }

    fun adapt(handler: (ServerRequest) -> Mono<Response>): (ServerRequest) -> Mono<ServerResponse> =
        { serverRequest ->
            handler(serverRequest)
                .flatMap { it.toWeb() }
                .logOnError { logger.warn(it.localizedMessage, it) }
                .logOnSuccess { logger.info("${serverRequest.path()} ${it.statusCode()}") }
                .onErrorResume { ServerError }
                .contextWrite {
                    it.put(TraceIdContextKey, serverRequest.getTraceId())
                }
        }

    private fun ServerRequest.getTraceId(): TraceId =
        // TraceId header is guaranteed to exist, see TraceIdFilter
        this.headers().firstHeader(TraceIdHeaderName)!!.let(::TraceId)
}
