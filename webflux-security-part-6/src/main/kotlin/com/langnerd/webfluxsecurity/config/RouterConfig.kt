package com.langnerd.webfluxsecurity.config

import com.langnerd.webfluxsecurity.dto.Response.Companion.AvatarChangeForm
import com.langnerd.webfluxsecurity.dto.Response.Companion.EmailLoginForm
import com.langnerd.webfluxsecurity.dto.Response.Companion.IndexRedirect
import com.langnerd.webfluxsecurity.dto.Response.Companion.LoginForm
import com.langnerd.webfluxsecurity.handler.*
import com.langnerd.webfluxsecurity.handler.email.ChangeEmailFormHandler
import com.langnerd.webfluxsecurity.handler.email.ChangeEmailHandler
import com.langnerd.webfluxsecurity.handler.email.EmailLoginHandler
import com.langnerd.webfluxsecurity.handler.user.UserAvatarHandler
import com.langnerd.webfluxsecurity.handler.user.UserHandler
import com.langnerd.webfluxsecurity.handler.user.UsersHandler
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.web.reactive.function.server.router

@Configuration
class RouterConfig(private val webAdapter: WebAdapter) {

    @Bean
    fun routes(
        userAvatarHandler: UserAvatarHandler,
        changeEmailHandler: ChangeEmailHandler,
        changeEmailFormHandler: ChangeEmailFormHandler,
        emailLoginHandler: EmailLoginHandler,
        indexHandler: IndexHandler,
        userHandler: UserHandler,
        usersHandler: UsersHandler,
    ) = router {
        GET("/", indexHandler.adapt())
        GET("/avatar/change") { AvatarChangeForm }
        POST("/avatar/change", userAvatarHandler.adapt())
        GET("/email/change", changeEmailFormHandler.adapt())
        POST("/email/change", changeEmailHandler.adapt())
        GET("/login") { LoginForm }
        GET("/login/email") { EmailLoginForm }
        POST("/login/email", emailLoginHandler.adapt())
        GET("/login/token/{token}") { IndexRedirect }
        GET("/users", usersHandler.adapt())
        GET("/users/{username}", userHandler.adapt())
    }

    private fun <T : Handler> T.adapt() =
        webAdapter.adapt(this::handle)

}