# Episode 6 - Mitigation of Common Security Threats

Objective: Shield against some common security threats described by OWASP Top Ten.

Coming soon!:
* YouTube
* Blog Post

Threats in scope:
* [Broken Access Control](#broken-access-control)
* [SQL Injection](#sql-injection)
* [Security Misconfiguration](#security-misconfiguration)
* [Identification and Authentication Failures](#identification-and-authentication-failures)
* [Server-Side Request Forgery](#server-side-request-forgery)

## How to Build and Run the Project

From the project root run the following command to build the project:
```
./gradlew :webflux-security-part-6:build
```

In order to run the application:
```
java -jar ./webflux-security-part-6/build/libs/webflux-security-part-6.jar
```

## Broken Access Control
This issue topped the list of OWASP Top Ten in 2021!

> Unauthorized information disclosure, modification, or destruction 
> of all data or performing a business function outside the user's limits.

[OWASP Top Ten: Broken Access Control](https://owasp.org/Top10/A01_2021-Broken_Access_Control)

### Scenario 1: Prevent unauthorized access to admin content

Only an admin should be able to access information about all users in the system at `/users`.

[SecurityConfig](src/main/kotlin/com/example/demo/config/SecurityConfig.kt):
```kotlin
http 
    .authorizeExchange()
    .pathMatchers("/login/**", "/*.css").permitAll()
    .pathMatchers("/users").hasAuthority("ADMIN")
```

### Scenario 2: Prevent unauthorized access to user details

Suppose an endpoint that displays user details at `/users/{username}`. The endpoint is only accessible to authenticated
users. However, without additional verification it can be easily misused.

For example, the user `bob` can view his own details by going to `/users/bob`. This is a legitimate use of the endpoint.
Nevertheless, Bob can also try to access details of his colleague, Alice, by visiting `/users/alice`, and that's not okay.

[UserHandler](src/main/kotlin/com/example/demo/handler/UserHandler.kt):
```kotlin
val username = req.pathVariable(UsernameVariable)

if (currentUser.username == username || currentUser.isAdmin) {
    // Go ahead and process the request
} else {
   // Reject the request as unauthorized and report the incident 
}
```

## SQL Injection
Remains one of the top threats described by OWASP Top Ten.

> User-supplied data is not validated, filtered, or sanitized by the application.

[OWASP Top Ten: Injection](https://owasp.org/Top10/A03_2021-Injection)

Suppose a page which displays details about one or several users - [user-overview.html](src/main/resources/templates/user-overview.html).

This view is primarily used by application admin, who views details of all users by going to `/users`. This route
is only available to administrators and the query takes no user input.

Unfortunately, the same view is also repurposed for displaying details of a single user at `/users/{username}`. 
Without validation the input parameter, `username`, can be misused for SQL injection.

### Scenario 1: Risk of unauthorized access to ALL users in the database
[UserHandler](src/main/kotlin/com/example/demo/handler/UserHandler.kt):

In the worst case scenario, the query is such that it creates an opening for returning ALL users from the database!

If we pass a legit username, such as `bob` or `alice`, the query below will consistently return a single record. However,
if we misuse the `username` parameter for some sort of wildcard trickery, it is possible that the query will return multiple
records or even all the users.

```kotlin
userRepository.findAllById(listOf(req.pathVariable(UsernameVariable))).collectList().map { users ->
    HtmlTemplate("user-overview", mapOf("users" to users))
}
```

### Scenario 2: Risk of unauthorized access to ANY single user in the database
Another vulnerable query potentially returning ANY (single) user from the database.

```kotlin
userRepository.findByUsername(req.pathVariable(UsernameVariable)).map { user ->
    HtmlTemplate("user-overview", mapOf("users" to listOf(user)))
}
```

This is slightly better, as now the query explicitly works with a single record. That's a step to the right direction, 
since imposing a limit on the number of returned records is a recommended practice. However, there is still no input
validation. The query would work for just any user in the database!

### Scenario 3: User input validation prevents any unauthorized access
The provided username is matched against the logged-in user. Unless the username matches or the logged-in user
is an admin, the request is rejected as unauthorized and the incident is reported.

```kotlin
val username = req.pathVariable(UsernameVariable)
if (currentUser.username == username || currentUser.isAdmin) {
    userRepository.findByUsername(username).map { user ->
        HtmlTemplate("user-overview", mapOf("users" to listOf(user)))
    }
} else {
    // Reject the request as unauthorized and report the incident 
}
```

## Security Misconfiguration
This is a broad topic, as the "misconfiguration" can mean anything from forgetting to change the default settings,
failure to gracefully handle application errors, outdated dependencies over to poor security practices overall.

Here is what I want to focus on in this demo:
> Missing appropriate security hardening across any part of the application stack

[OWASP Top Ten: Security Misconfiguration](https://owasp.org/Top10/A05_2021-Security_Misconfiguration)

### Scenario: Neglected misconfiguration leaving the app vulnerable to CSRF

When starting with Spring Security, it is sometimes convenient to temporarily disable CSRF protection.

[SecurityConfig](src/main/kotlin/com/example/demo/config/SecurityConfig.kt):
```kotlin
http.csrf().disable()   // Now it's easy to submit just about any HTML form
    .authorizeExchange()
```

If we forget to revert this change, then the application is susceptible to [Cross Site Request Forgery](https://owasp.org/www-community/attacks/csrf).

Suppose the user has a possibility to change their email address by visiting `email/change` as outlined in [change-email.html](src/main/resources/templates/change-email.html).
Once the user submits the form, the user's email is set to the newly provided email address.

Imagine, the attacker tricks the user into visiting a malicious website forcing the user to change their email
to the one owned by the attacker. If this attempt is successful the attacker will most likely gain a complete 
control over the user's account. Once the request is made - from the malicious site to our server - without CSRF token the server has now way of discerning a fraudulent request from
the legitimate one.

The mitigation is easy:
1. Remove `http.csrf().disable()` from the config
2. Add security extras in [build.gradle.kts](build.gradle.kts)
```groovy
implementation("org.thymeleaf.extras:thymeleaf-extras-springsecurity5:$thymeLeafExtrasVersion")
``` 

Spring Security provides CSRF protection by default, so removing the outstanding line that disables this check is enough.

For convenience, we've added [Thymeleaf Extras for Spring Security](https://github.com/thymeleaf/thymeleaf-extras-springsecurity). 
Meaning, all form submissions will automatically include a `_csrf` token. This token is generated on the server side. If the client request lacks this parameter, or if it does not match the value
set by the server, then the server knows the request cannot be trusted and rejects it.

## Identification and Authentication Failures

Again, this is a broad topic that touches upon many issues. Starting with weak passwords, missing multi-factor authentication,
over to user session management, secure recovery of user's credentials etc.

Reliable expiry of user sessions is one of the simplest steps that help mitigate the risk.

> User sessions or authentication tokens (mainly single sign-on (SSO) tokens) aren't properly invalidated during logout or a period of inactivity.

[OWASP Top Ten: Identification and Authentication Failures](https://owasp.org/Top10/A07_2021-Identification_and_Authentication_Failures)

### Scenario: Ensure the User Session is Invalidated

#### Step 1: Prepare configuration

Suppose the user session should be invalidated after 15 minutes of inactivity.

[application.yaml](src/main/resources/application.yaml):
```yaml
app:
  security:
    session:
      timeout: 15m
```

#### Step 2: Capture the new configuration in the code

Translate `yaml` config into a data class. 

[SessionProperties](src/main/kotlin/com/example/demo/config/SessionProperties.kt):

```kotlin
package com.langnerd.webfluxsecurity.config

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.ConstructorBinding
import java.time.Duration

@ConstructorBinding
@ConfigurationProperties(prefix = "app.security.session")
data class SessionProperties(val timeout: Duration)
```

#### Step 3: Plug the session timeout into the security configuration

Provide a custom session repository. The one shown here, is the default in-memory implementation. For your app to scale,
it's better to persist user session into a storage, such as Redis. This is beyond this tutorial, but there are plenty of examples out there.

```kotlin
@Bean
fun sessionRepository(sessionProperties: SessionProperties): ReactiveMapSessionRepository { // Helps prevent authentication failures by reliably expiring the user session: https://owasp.org/Top10/A07_2021-Identification_and_Authentication_Failures
    val sessionRepository = ReactiveMapSessionRepository(ConcurrentHashMap())
    sessionRepository.setDefaultMaxInactiveInterval(sessionProperties.timeout.seconds.toInt())
    return sessionRepository
}
```

### Server-Side Request Forgery
Another threat with potentially dire consequences if the user input is not validated and sanitized.

> SSRF flaws occur whenever a web application is fetching a remote resource without validating the user-supplied URL.

[OWASP Top Ten: Server-Side Request Forgery](https://owasp.org/Top10/A10_2021-Server-Side_Request_Forgery_%28SSRF%29)

### Scenario: URL Validation when Uploading User's Avatar

Suppose users are able to upload their avatars. Apart fromchoosing a file from their local filesystem, the users 
are also allowed to copy-n-paste a link to the file. Without any validation (and sanitization) in place, a user
with malicious intents could upload a link to an infected file from just about anywhere on the Internet.

Here is how the risk was mitigated.

#### Step 1: Decide on whitelisted domains

Only images from `imgur.com` and `pixabay.com` are accepted.

[application.yaml](src/main/resources/application.yaml):
```yaml
app:
  security:
    avatar:
      whitelisted-domains:
        - https://imgur.com
        - https://pixabay.com
```

#### Step 2: Transform the config into code

Note, how each domain entry is automatically converted into `java.net.URI`.

[AvatarProperties](src/main/kotlin/com/example/demo/config/AvatarProperties.kt):

```kotlin
@ConstructorBinding
@ConfigurationProperties(prefix = "app.security.avatar")
data class AvatarProperties(val whitelistedDomains: List<URI>)
```

#### Step 3: Apply the whitelist when the user submits the requested link

Each link, valid or not, before it is saved or printed out to the log, is properly escaped. This is a form of sanitization.

[ChangeAvatarHandler](src/main/kotlin/com/example/demo/handler/ChangeAvatarHandler.kt):

```kotlin
if (isWhitelistedDomain(uri)) {
    val updatedUser = user.copy(avatar = encode(uri))
    userRepository.save(updatedUser)
        .map { HtmlTemplate("user-overview", mapOf("users" to listOf(updatedUser))) }
} else {
    logger.warn("User ${user.username} tried to upload file \"${encode(uri)}\"")
    HtmlTemplate(
        "change-avatar",
        mapOf("error" to "Invalid link. The incident has been reported.")
    ).toMono()
}

private fun encode(uri: URI): String =
    URLEncoder.encode(uri.toString(), StandardCharsets.UTF_8)

private fun isWhitelistedDomain(uri: URI): Boolean =
    avatarProperties.whitelistedDomains.any { uri.toString().startsWith(it.toString()) }
```

## Reference Documentation and Guides
For further reference, please consider the following articles and guides:

* [Spring Security](https://docs.spring.io/spring-boot/docs/2.5.6/reference/htmlsingle/#boot-features-security)
* [Spring Security with WebFlux](https://docs.spring.io/spring-security/reference/reactive/configuration/webflux.html)
* [OWASP Top Ten](https://owasp.org/www-project-top-ten)

