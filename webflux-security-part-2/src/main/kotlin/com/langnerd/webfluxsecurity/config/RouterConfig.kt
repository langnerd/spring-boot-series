package com.langnerd.webfluxsecurity.config

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.MediaType
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.reactive.function.server.ServerRequest
import org.springframework.web.reactive.function.server.ServerResponse
import org.springframework.web.reactive.function.server.router
import reactor.core.publisher.Mono

@Controller
class LoginController {

    @GetMapping("/login")
    fun handleLogin(): Mono<String> = Mono.just("login")
}

@Configuration
class RouterConfig {

    @Bean
    fun routes() = router {
        GET("/login")(::handleLogin)
    }

    private fun handleLogin(req: ServerRequest): Mono<ServerResponse> =
        ServerResponse.ok().contentType(MediaType.TEXT_HTML).render("login")

}

