package com.langnerd.webfluxsecurity

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.context.properties.ConfigurationPropertiesScan
import org.springframework.boot.runApplication

@SpringBootApplication
@ConfigurationPropertiesScan
class App02

fun main(args: Array<String>) {
	runApplication<App02>(*args)
}
