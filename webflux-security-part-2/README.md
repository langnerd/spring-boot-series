# Episode 2 - Custom User Object

Objective: Customize the user object by adding properties such as first and last name, or an email address and load users from a database.

* YouTube
* Blog Post

## How to Build and Run the Project

From the project root run the following command to build the project:
```
./gradlew :webflux-security-part-2:build
```

In order to run the application:
```
java -jar ./webflux-security-part-2/build/libs/webflux-security-part-2.jar
```

### Reference Documentation and Guides
For further reference, please consider the following articles and guides:

* [Spring Security](https://docs.spring.io/spring-security/reference/)
* [Spring Security with WebFlux](https://docs.spring.io/spring-security/reference/reactive/configuration/webflux.html)
* [Authentication with a Database-backed UserDetailsService](https://www.baeldung.com/spring-security-authentication-with-a-database)

