# Episode 7 - Two-Factor Authentication

Objective: Add an option to use two-factor authentication (2FA). 

Coming soon!:
* YouTube
* Blog Post

## How to Build and Run the Project

From the project root run the following command to build the project:
```
./gradlew :webflux-security-part-7:build
```

In order to run the application:
```
java -jar ./webflux-security-part-7/build/libs/webflux-security-part-7.jar \
--spring.config.location=classpath:/application.yaml,classpath:/override.yaml
```

## Reference Documentation and Guides
For further reference, please consider the following articles and guides:

* [Spring Security](https://docs.spring.io/spring-boot/docs/2.5.6/reference/htmlsingle/#boot-features-security)
* [Spring Security with WebFlux](https://docs.spring.io/spring-security/reference/reactive/configuration/webflux.html)
* [OWASP Top Ten](https://owasp.org/www-project-top-ten)

