package com.langnerd.webfluxsecurity.config

import com.langnerd.webfluxsecurity.filter.TwoFactorAuthenticationFilter
import com.langnerd.webfluxsecurity.filter.TokenAuthenticationFilter
import com.langnerd.webfluxsecurity.service.EmailTokenService
import org.springframework.context.annotation.Bean
import org.springframework.http.HttpMethod
import org.springframework.security.authentication.ReactiveAuthenticationManager
import org.springframework.security.authentication.UserDetailsRepositoryReactiveAuthenticationManager
import org.springframework.security.config.annotation.web.reactive.EnableWebFluxSecurity
import org.springframework.security.config.web.server.SecurityWebFiltersOrder
import org.springframework.security.config.web.server.ServerHttpSecurity
import org.springframework.security.core.userdetails.ReactiveUserDetailsService
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.security.web.server.SecurityWebFilterChain
import org.springframework.security.web.server.context.WebSessionServerSecurityContextRepository
import org.springframework.security.web.server.csrf.WebSessionServerCsrfTokenRepository
import org.springframework.security.web.server.util.matcher.ServerWebExchangeMatchers
import org.springframework.session.config.annotation.web.server.EnableSpringWebSession


@EnableWebFluxSecurity
@EnableSpringWebSession
class SecurityConfig {

    @Bean
    fun springSecurityFilterChain(
        http: ServerHttpSecurity,
        tokenAuthenticationFilter: TokenAuthenticationFilter,
        authenticationManager: ReactiveAuthenticationManager
    ): SecurityWebFilterChain =
        http    // CSRF protection is enabled by default. Beware of vulnerabilities caused by security misconfiguration: https://owasp.org/Top10/A05_2021-Security_Misconfiguration
            .authorizeExchange()
            .pathMatchers("/login/**", "/*.css").permitAll()
            .pathMatchers("/users")
            .hasAuthority("ADMIN")    // Prevents unauthorized access! See https://owasp.org/Top10/A01_2021-Broken_Access_Control
            .anyExchange().authenticated()
            .and().csrf().csrfTokenRepository(WebSessionServerCsrfTokenRepository())
            .and().authenticationManager(authenticationManager)
            .addFilterBefore(tokenAuthenticationFilter, SecurityWebFiltersOrder.AUTHENTICATION)
            .formLogin().loginPage("/login").authenticationSuccessHandler(TwoFactorAuthenticationFilter())
            .and().oauth2Login()
            .and().logout()
            .requiresLogout(ServerWebExchangeMatchers.pathMatchers(HttpMethod.GET, "/logout"))
            .and().build()

    @Bean
    fun tokenAuthenticationFilter(emailTokenService: EmailTokenService): TokenAuthenticationFilter =
        TokenAuthenticationFilter(emailTokenService, WebSessionServerSecurityContextRepository())

    @Bean
    fun authenticationManager(userDetailsService: ReactiveUserDetailsService): ReactiveAuthenticationManager {
        val authenticationManager = UserDetailsRepositoryReactiveAuthenticationManager(userDetailsService)
        authenticationManager.setPasswordEncoder(passwordEncoder())
        return authenticationManager
    }

    private fun passwordEncoder(): PasswordEncoder =
        BCryptPasswordEncoder(10)
}