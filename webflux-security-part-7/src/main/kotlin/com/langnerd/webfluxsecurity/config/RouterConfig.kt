package com.langnerd.webfluxsecurity.config

import com.langnerd.webfluxsecurity.dto.Response.Companion.AvatarChangeForm
import com.langnerd.webfluxsecurity.dto.Response.Companion.EmailLoginForm
import com.langnerd.webfluxsecurity.dto.Response.Companion.IndexRedirect
import com.langnerd.webfluxsecurity.dto.Response.Companion.LoginForm
import com.langnerd.webfluxsecurity.dto.Response.Companion.TwoFactorCodeForm
import com.langnerd.webfluxsecurity.handler.Handler
import com.langnerd.webfluxsecurity.handler.IndexHandler
import com.langnerd.webfluxsecurity.handler.WebAdapter
import com.langnerd.webfluxsecurity.handler.email.ChangeEmailFormHandler
import com.langnerd.webfluxsecurity.handler.email.ChangeEmailHandler
import com.langnerd.webfluxsecurity.handler.email.EmailLoginHandler
import com.langnerd.webfluxsecurity.handler.twofactor.OneTimePasswordHandler
import com.langnerd.webfluxsecurity.handler.twofactor.TwoFactorSetupConfirmHandler
import com.langnerd.webfluxsecurity.handler.twofactor.TwoFactorSetupFormHandler
import com.langnerd.webfluxsecurity.handler.twofactor.TwoFactorSetupHandler
import com.langnerd.webfluxsecurity.handler.user.UserAvatarHandler
import com.langnerd.webfluxsecurity.handler.user.UserHandler
import com.langnerd.webfluxsecurity.handler.user.UsersHandler
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.web.reactive.function.server.router

@Configuration
class RouterConfig(private val webAdapter: WebAdapter) {

    @Bean
    fun routes(
        userAvatarHandler: UserAvatarHandler,
        changeEmailHandler: ChangeEmailHandler,
        changeEmailFormHandler: ChangeEmailFormHandler,
        emailLoginHandler: EmailLoginHandler,
        indexHandler: IndexHandler,
        twoFactorSetupHandler: TwoFactorSetupHandler,
        twoFactorSetupConfirmHandler: TwoFactorSetupConfirmHandler,
        twoFactorSetupFormHandler: TwoFactorSetupFormHandler,
        oneTimePasswordHandler: OneTimePasswordHandler,
        userHandler: UserHandler,
        usersHandler: UsersHandler,
    ) = router {
        GET("/", indexHandler.adapt())
        GET("/2fa/setup", twoFactorSetupFormHandler.adapt())
        GET("/2fa/setup/confirm", twoFactorSetupConfirmHandler.adapt())
        GET("/avatar/change") { AvatarChangeForm }
        GET("/email/change", changeEmailFormHandler.adapt())
        GET("/login") { LoginForm }
        GET("/login/code") { TwoFactorCodeForm }
        GET("/login/email") { EmailLoginForm }
        GET("/login/token/{token}") { IndexRedirect }
        GET("/users", usersHandler.adapt())
        GET("/users/{username}", userHandler.adapt())
        POST("/", indexHandler.adapt()) // Redirect to the home page
        POST("/avatar/change", userAvatarHandler.adapt())
        POST("/email/change", changeEmailHandler.adapt())
        POST("/login/email", emailLoginHandler.adapt())
        POST("/2fa/setup", twoFactorSetupHandler.adapt())
        POST("/login/code", oneTimePasswordHandler.adapt())
    }

    private fun <T : Handler> T.adapt() = webAdapter.adapt(this::handle)
}